#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/string.h>

#define CMD_NONE 0x0
#define CMD_DRAW_LINE 0x2
#define CMD_DRAW_TEXT 0x4
#define CMD_UPDATE 0x8

uint8_t get_cmd(const char *str, char **next);
uint16_t get_arg_int(const char *str, char **next);
uint16_t get_arg_string(const char *str, char **next);