#include "cmd_parser.h"

static char* find_next(const char *str) {
	char *next = str;
	while (*next != ';' && *next != '\0') {
		next++;
	}
	if (*next == ';') {
		next++;
	}
	return next;
}

uint8_t get_cmd(const char *str, char **next) {
	uint8_t cmd = CMD_NONE;
	if (!strncmp(str, "line", 4)) {
		cmd = CMD_DRAW_LINE;
	} else if (!strncmp(str, "text", 4)) {
		cmd = CMD_DRAW_TEXT;
	} else if (!strncmp(str, "updt", 4)) {
		cmd = CMD_UPDATE;
	}
	*next = find_next(str);
	return cmd;
}

uint16_t get_arg_int(const char *str, char **next) {
	char snum[6];
	size_t size = 0;
	for (; str[size] != ';' && str[size] != '\0' && size < 5; ++size);
	strncpy(snum, str, size);
	snum[size] = '\0';
	uint32_t res = 0;
	kstrtouint(snum, 10, &res);
	*next = find_next(str);
	return res;
}

uint16_t get_arg_string(const char *str, char **next) {
	size_t size = 0;
	for (; str[size] != ';' && str[size] != '\0'; ++size);
	*next = find_next(str);
	return size;
}