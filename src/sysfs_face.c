#include "sysfs_face.h"

static st7735_t *display;

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,32)
static ssize_t inversion_store(struct class *class, struct class_attribute *attr, const char *buf, size_t count) {
#else
static ssize_t inversion_store(struct class *class, const char *buf, size_t count) {
#endif
	st_inversion(display, buf[0] - '0');
	return count;
}
CLASS_ATTR_WO(inversion);

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,32)
static ssize_t clear_store(struct class *class, struct class_attribute *attr, const char *buf, size_t count) {
#else
static ssize_t clear_store(struct class *class, const char *buf, size_t count) {
#endif
	st_clear(display, (rgb_color_t){255, 255, 255});
	st_present(display, 0, 0, display->width, display->height);
	return count;
}
CLASS_ATTR_WO(clear);

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,32)
static ssize_t cmd_store(struct class *class, struct class_attribute *attr, const char *buf, size_t count) {
#else
static ssize_t cmd_store(struct class *class, const char *buf, size_t count) {
#endif
	char *next;
	uint8_t cmd = get_cmd(buf, &next);
	if (cmd == CMD_DRAW_LINE) {
		uint16_t x0, y0, x1, y1;
		x0 = get_arg_int(next, &next);
		y0 = get_arg_int(next, &next);
		x1 = get_arg_int(next, &next);
		y1 = get_arg_int(next, &next);
		printk(KERN_INFO "LINE: (%d, %d) (%d, %d)\n", x0, y0, x1, y1);
		st_draw_line(display, x0, y0, x1, y1, (rgb_color_t){0, 0, 0});
	} else if (cmd == CMD_UPDATE) {
		uint16_t x, y, w, h;
		x = get_arg_int(next, &next);
		y = get_arg_int(next, &next);
		w = get_arg_int(next, &next);
		h = get_arg_int(next, &next);
		printk(KERN_INFO "UPDATE: (%d, %d) (%d, %d)\n", x, y, w, h);
		st_present(display, x, y, w, h);
	} else if (cmd == CMD_DRAW_TEXT) {
		uint16_t x, y, size;
		x = get_arg_int(next, &next);
		y = get_arg_int(next, &next);
		char *s = next;
		size = get_arg_string(next, &next);
		char *str = (char*)kmalloc(size + 1, GFP_KERNEL);
		if (str != NULL) {
			strncpy(str, s, size);
			str[size] = '\0';
			st_put_string(display, str, x, y, (rgb_color_t){0, 0, 0});
			kfree(str);
		}
	}
	return count;
}
CLASS_ATTR_WO(cmd);

static struct class *dspdrv_class;

int sysfs_face_init(st7735_t *st) {
	display = st;
	int res = 0;
	dspdrv_class = class_create(THIS_MODULE, "uniac-dsp");
	if (IS_ERR(dspdrv_class)) {
		res = -1;
		goto err;
	}
	res = class_create_file(dspdrv_class, &class_attr_inversion);
	res += class_create_file(dspdrv_class, &class_attr_clear);
	res += class_create_file(dspdrv_class, &class_attr_cmd);
err:
	return res;
}

void sysfs_face_exit(void) {
	class_remove_file(dspdrv_class, &class_attr_inversion);
	class_remove_file(dspdrv_class, &class_attr_clear);
	class_remove_file(dspdrv_class, &class_attr_cmd);
	class_destroy(dspdrv_class);
}