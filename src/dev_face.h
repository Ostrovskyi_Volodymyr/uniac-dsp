#ifndef DEV_FACE_H_
#define DEV_FACE_H_

#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/version.h>      /* LINUX_VERSION_CODE */
#include <linux/fs.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

#include "st7735/st7735.h"

int dev_face_init(st7735_t *st);
void dev_face_exit(void);

#endif // DEV_FACE_H_