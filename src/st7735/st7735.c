#include "st7735.h"

int st_init(st7735_t *st, struct spi_device *spi, uint8_t DCpin) {
	st->st_dc.gpio = DCpin;
	st->st_dc.flags = GPIOF_OUT_INIT_LOW;
	st->st_dc.label = "Display Data/Command";

	int ret = gpio_request(st->st_dc.gpio, st->st_dc.label);
	if (ret < 0) {
		printk(KERN_INFO "%s(): GPIO request failed!\n", __func__);
		return ret;
	}
	gpio_direction_output(st->st_dc.gpio, 0);
	st->spi_dev = spi;
	st->width = 128;
	st->height = 160;
	st->framebuf_size = st->width * st->height * 3;
	st->framebuf = (uint8_t*)kmalloc((size_t)st->framebuf_size, GFP_KERNEL);
	// INIT
	st_cmd(st, SWRESET);
	mdelay(200);
	st_cmd(st, SLPOUT);
	mdelay(200);

	//st_cmd(st, PWCTR3);
	//st_write_byte(st, PWR3_AP_SML, 1);
	//st_write_byte(st, PWR3_DC_1_2, 1);
	st_cmd(st, MADCTL);
	st_write_byte(st, MAD_RGB, 1);
	st_cmd(st, COLMOD);
	st_write_byte(st, COLOR_18BIT, 1);

	st_cmd(st, DISPON);
	mdelay(200);
	return 0;
}

void st_delete(st7735_t *st) {
	gpio_free(st->st_dc.gpio);
	kfree(st->framebuf);
}

void st_write_byte(st7735_t *st, uint8_t b, uint8_t dc) {
	gpio_set_value(st->st_dc.gpio, dc);
	spi_write(st->spi_dev, &b, sizeof(uint8_t));
}

void st_write_word(st7735_t *st, uint16_t w) {
	st_write_byte(st, w >> 8, 1);
	st_write_byte(st, w & 0xFF, 1);
}

void st_cmd(st7735_t *st, uint8_t cmd) {
	st_write_byte(st, cmd, 0);
}

void st_window(st7735_t *st, uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1) {
	st_cmd(st, CASET);
	st_write_word(st, x0);
	st_write_word(st, x1);
	st_cmd(st, RASET);
	st_write_word(st, y0);
	st_write_word(st, y1);
}

static void _blit_pxl(uint8_t *framebuf, rgb_color_t color) {
	framebuf[0] = color.r;
	framebuf[1] = color.g;
	framebuf[2] = color.b;
}

void st_draw_pixel(st7735_t *st, uint16_t x, uint16_t y, rgb_color_t color) {
/*	st_window(st, x, y, x, y);
	st_cmd(st, RAMWR);
	st_write888(st, color, 1);
*/	if (x < st->width && y < st->height) {
		_blit_pxl(&(st->framebuf[x * 3 + st->width * 3 * y]), color);
	}
}

void st_fill_rect(st7735_t *st, uint16_t x, uint16_t y, uint16_t w, uint16_t h, rgb_color_t color) {
/*	uint16_t x1 = w + x - 1;
	uint16_t y1 = h + y - 1;
	st_window(st, x, y, x1, y1);
	st_cmd(st, RAMWR);
*/
	size_t i = y;
	for (; i < h; ++i) {
		size_t j = x;
		for (; j < w; ++j) {
			st_draw_pixel(st, j, i, color);
		}
	}
	//st_write888(st, color, w*h);
}

void st_present(st7735_t *st, uint16_t x, uint16_t y, uint16_t w, uint16_t h) {
	if (w + x > st->width) {
		w = st->width - x;
	}
	if (h + y > st->height) {
		h = st->height - y;
	}
	st_window(st, x, y, w - 1 + x, h - 1 + y);
	st_cmd(st, RAMWR);
	gpio_set_value(st->st_dc.gpio, 1);
	size_t i = y;
	for (; i < y + h; ++i) {
		spi_write(st->spi_dev, &st->framebuf[x * 3 + i * st->width * 3], w * 3);
	}
}

void st_clear(st7735_t *st, rgb_color_t color) {
	st_fill_rect(st, 0, 0, st->width, st->height, color);
	//memset(st->framebuf, 255, st->framebuf_size);
}

void st_inversion(st7735_t *st, uint8_t on) {
	if (on) {
		st_cmd(st, INVON);
	} else {
		st_cmd(st, INVOFF);
	}
}

void st_put_char(st7735_t *st, char c, uint16_t x, uint16_t y, rgb_color_t color) {
	if (c >= 'a' && c <= 'z') {
		c -= 32;
	}
	uint8_t *g = &(font8x10.glyph[(c - font8x10.start_c) * 10]);
	size_t i = 0;
	uint8_t mask = 1 << (font8x10.width - 1);
	for (; i < font8x10.height; ++i) {
		size_t j = 0;
		for (; j < font8x10.width; ++j) {
			if ((g[i] << j) & mask) {
				st_draw_pixel(st, x + j, y + i, color);
			}
		}
	}
}

void st_put_string(st7735_t *st, const char *str, uint16_t x, uint16_t y, rgb_color_t color) {
	uint16_t px = x, py = y;
	size_t i = 0;
	for (; str[i]; ++i) {
		if (str[i] == '\n' || (px + font8x10.width + 1) >= st->width) {
			if ((py + font8x10.height + 1) >= st->height)
				break;
			py += font8x10.height + 1;
			px = x;
			if (str[i] == '\n')
				continue;
		}
		if (str[i] != ' ') {
			st_put_char(st, str[i], px, py, color);
		}
		px += font8x10.width + 1;
	}
}

void st_put_int(st7735_t *st, int num, uint16_t x, uint16_t y, rgb_color_t color) {
	char sn[11] = {0};
	int i = 9;
	while (num) {
		sn[i] = (num % 10) + '0';
		num /= 10;
		--i;
	}
	++i;
	st_put_string(st, sn + i, x, y, color);
}

#define ABS(a) ((a) < 0 ? -(a) : (a))
#define SWAP(a, b) 	\
{					\
	int t = a;		\
	a = b;			\
	b = t;			\
}					\

void st_draw_line(st7735_t *st, uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, rgb_color_t color) {
	int deltax = ABS((int)x1 - (int)x0);
	int deltay = ABS((int)y1 - (int)y0);
	
	if (x0 > x1 || y0 > y1) {
		SWAP(x0, x1)
		SWAP(y0, y1)
	}

	if (deltax >= deltay) {
		int diry = (int)y1 - (int)y0;
		if (diry > 0) {
			diry = 1;
		} else if (diry < 0) {
			diry = -1;
		}
		int err = 0;
		int deltaerr = deltay + 1;
		int y = y0;
		int x = x0;
		for (; x <= x1; ++x) {
			st_draw_pixel(st, x, y, color);
			err += deltaerr;
			if (err >= deltax + 1) {
				y += diry;
				err = err - (deltax + 1);
			}
		}
	} else {
		int dirx = (int)x1 - (int)x0;
		if (dirx > 0) {
			dirx = 1;
		} else if (dirx < 0) {
			dirx = -1;
		}
		int err = 0;
		int deltaerr = deltax + 1;
		int x = x0;
		int y = y0;
		for (; y <= y1; ++y) {
			st_draw_pixel(st, x, y, color);
			err += deltaerr;
			if (err >= deltay + 1) {
				x += dirx;
				err = err - (deltay + 1);
			}
		}
	}
}