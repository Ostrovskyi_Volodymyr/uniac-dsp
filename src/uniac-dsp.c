#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/spi/spi.h>
#include <linux/device.h>
#include <linux/of.h>
#include <linux/gpio.h>
#include <linux/slab.h>

#include "dev_face.h"
#include "sysfs_face.h"
#include "st7735/st7735.h"

#define RS_PIN 22

static st7735_t display;

static const struct of_device_id dsp_match_id[] = {
	{.compatible="uniac,dsp",},
	{},
};
MODULE_DEVICE_TABLE(of, dsp_match_id);

static int dsp_probe(struct spi_device *spi_dev) {
	spi_dev->mode = SPI_MODE_0 | SPI_CS_WORD;
	spi_dev->max_speed_hz = 20000000;
	spi_dev->bits_per_word = 8;
	int ret = spi_setup(spi_dev);
	if (ret < 0) {
		printk(KERN_INFO "%s(): SPI setup failed!\n", __func__);
		return ret;
	} else {
		printk(KERN_INFO "%s(): Successfully setup SPI\n", __func__);
	}
	const char *devname;
	if (!of_property_read_string(spi_dev->dev.of_node, "device-name", &devname)) {
		printk(KERN_INFO "%s(): Device name: %s\n", __func__, devname);
	}
	
	st_init(&display, spi_dev, RS_PIN);
	st_clear(&display, (rgb_color_t){255, 255, 255});
	st_put_string(&display, "The potato is a root vegetable native to the Americas,"
							"a starchy tuber of the plant Solanum tuberosum, and the "
							"plant itself is a perennial in the nightshade family, Solanaceae", 
							0, 0, (rgb_color_t){0, 0, 0});
	st_draw_line(&display, 83, 139, 95, 154, (rgb_color_t){200, 0, 0});
	st_draw_line(&display, 95, 154, 71, 154, (rgb_color_t){200, 0, 0});
	st_draw_line(&display, 71, 154, 83, 139, (rgb_color_t){200, 0, 0});
	st_draw_line(&display, 103, 150, 115, 135, (rgb_color_t){0, 0, 200});
	st_draw_line(&display, 115, 135, 91, 135, (rgb_color_t){0, 0, 200});
	st_draw_line(&display, 91, 135, 103, 150, (rgb_color_t){0, 0, 200});
	st_present(&display, 0, 0, display.width, display.height);
	dev_face_init(&display);
	sysfs_face_init(&display);
	return ret;
}

static int dsp_remove(struct spi_device *spi_dev) {
	printk(KERN_INFO "Remove SPI driver\n");
	sysfs_face_exit();
	dev_face_exit();
	st_delete(&display);
	return 0;
}

struct spi_driver dsp_driver = {
	.probe = dsp_probe,
	.remove = dsp_remove,
	.driver = {
		.name = "uniac-dsp",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(dsp_match_id),
	},
};

module_spi_driver(dsp_driver);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("4umber");