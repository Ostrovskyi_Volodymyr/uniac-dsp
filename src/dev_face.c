#include "dev_face.h"

#define DEVICE_FIRST  0
#define DEVICE_COUNT  1
#define MODNAME "uniac_dsp"

static struct cdev hcdev;
static struct class *devclass;
static st7735_t *display;

static int major = 0;

static ssize_t dev_write(struct file * file, const char* buf, size_t count, loff_t *ppos);
static int dev_open(struct inode *inode, struct file *file) { return 0; }
static int dev_release(struct inode *inode, struct file *file) { return 0; }

static const struct file_operations dev_fops = {
   .owner = THIS_MODULE,
   .write = dev_write,
   .open = dev_open,
   .release = dev_release,
};

static ssize_t dev_write(struct file * file, const char* buf, size_t count, loff_t *ppos) {
	printk(KERN_INFO "count %d\n", count);
	if (*ppos + count <= display->framebuf_size) {
		raw_copy_from_user(display->framebuf + *ppos, buf, count);
	}
	st_present(display, 0, 0, display->width, display->height);
	printk(KERN_INFO "write %d\n", count);
	*ppos += count;
	return count;
}

int dev_face_init(st7735_t *st) {
	display = st;
	int ret;
	dev_t dev;
	ret = alloc_chrdev_region( &dev, DEVICE_FIRST, DEVICE_COUNT, MODNAME );
	major = MAJOR(dev);  // не забыть зафиксировать!

	if( ret < 0 ) {
    	printk( KERN_ERR "=== Can not register char device region\n" );
    	goto err;
   	}
	cdev_init( &hcdev, &dev_fops );
	hcdev.owner = THIS_MODULE;
	ret = cdev_add( &hcdev, dev, DEVICE_COUNT );
	if( ret < 0 ) {
    	unregister_chrdev_region( MKDEV( major, DEVICE_FIRST ), DEVICE_COUNT );
    	printk( KERN_ERR "=== Can not add char device\n" );
    	goto err;
   	}
	devclass = class_create( THIS_MODULE, "uniacdsp_class" ); /* struct class* */
    dev = MKDEV( major, DEVICE_FIRST );
#if LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,26)
      device_create( devclass, NULL, dev, "frame");
#else
      device_create( devclass, NULL, dev, NULL, "frame");
#endif
   printk( KERN_INFO "======== module installed %d:[%d-%d] ===========\n",
           MAJOR( dev ), DEVICE_FIRST, MINOR( dev ) ); 
err:
   return ret;
}

void  dev_face_exit( void ) {
	dev_t dev;
	int i;
	for( i = 0; i < DEVICE_COUNT; i++ ) {
		dev = MKDEV( major, DEVICE_FIRST + i );
    	device_destroy( devclass, dev );
	}
	class_destroy( devclass );
	cdev_del( &hcdev );
	unregister_chrdev_region( MKDEV( major, DEVICE_FIRST ), DEVICE_COUNT );
	printk( KERN_INFO "=============== module removed ==================\n" );
}