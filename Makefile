KMOD=uniac-dsp.ko
KDEV=uniac-dsp-overlay
DIR=$(PWD)
BUILD_KERNEL=/lib/modules/`uname -r`/build
OBJS=src/cmd_parser.o src/sysfs_face.o src/dev_face.o src/st7735/st7735.o src/st7735/font.o src/uniac-dsp.o
obj-m += uniac-dsp.o
uniac-dsp-objs:=$(OBJS)

all:
	make -C ${BUILD_KERNEL} M=${DIR} modules
	mkdir -p ${DIR}/bin
	mv ${DIR}/${KMOD} ${DIR}/bin/${KMOD}
	dtc -@ -I dts -O dtb -o ${DIR}/bin/${KDEV}.dtbo ${DIR}/src/${KDEV}.dts
	dtc -@ -I dts -O dtb -o ${DIR}/bin/spidev0-dis-overlay.dtbo ${DIR}/src/spidev0-dis-overlay.dts

clean:
	make -C ${BUILD_KERNEL} M=${DIR} clean
	rm -f ${DIR}/bin/${KDEV}.dtbo
	rm -f ${DIR}/bin/spidev0-dis-overlay.dtbo